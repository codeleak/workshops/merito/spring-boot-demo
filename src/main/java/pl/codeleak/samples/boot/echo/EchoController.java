package pl.codeleak.samples.boot.echo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class EchoController {
    @GetMapping("/echo")
    public String echo(@RequestParam("message") Optional<String> message, Model model) {
        model.addAttribute("message", message.orElse("Hello, World!"));
        return "echo";
    }
}
