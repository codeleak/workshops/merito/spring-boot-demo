# Spring Boot Demo

> Generated with https://start.spring.io


## Pre-requisites

Java 17


## Running the application

> Note, the Maven Wrapper is already included in the project, so a Maven install is not actually needed.
> You may first need to execute `chmod +x mvnw`.

```
./mvnw spring-boot:run
```

Once the runtime starts, you can access the project at http://localhost:8080
